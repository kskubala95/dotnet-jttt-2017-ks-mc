﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public class Log
    {
        public static void WriteErrorLog(string strErrorText)
        {
            try
            {
                //DECLARE THE FILENAME FROM THE ERROR LOG
                string strFileName = "Log_file.txt";
                /*DECLARE THE FOLDER WHERE THE LOGFILE HAS TO BE  STORED, IN THIS EXAMPLE WE CHOSE THE PATH OF THE CURRENT APPLICATION*/
                string strPath = Application.StartupPath;
                //WRITE THE ERROR TEXT AND THE CURRENT DATE-TIME TO THE ERROR FILE
                System.IO.File.AppendAllText(strPath + "\\" + strFileName, strErrorText + " - " + DateTime.Now.ToString() + "\r\n");
            }
            catch (Exception ex)
            {
                WriteErrorLog("Error in WriteErrorLog: " + ex.Message);
            }
        }

    }
}

