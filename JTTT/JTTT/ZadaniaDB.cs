﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Zadanie
    {
        public Zadanie()
        {

        }
        //public int StandardId { get; set; }
        //public string StandardName { get; set; }

        public int ZadanieID { get; set; }
        public string NazwaZadania { get; set; }
        public ICollection<Email> Emaile { get; set; }
        public ICollection<adresURL> Adresy { get; set; }


    }
}
