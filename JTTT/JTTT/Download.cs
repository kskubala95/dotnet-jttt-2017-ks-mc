﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class Download
    {
        const string IMAGES_DIRECTORY = "../../images/";

        public string DownloadImageAndGetLocalPath(string imageUrl)
        {
            string localImagePath = "";

            localImagePath = GetLocalImagePath(imageUrl);
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                client.DownloadFile(imageUrl, localImagePath);
                Log.WriteErrorLog("Pobrano obraz lokalnie.");

            }

            Log.WriteErrorLog("Zwrócono ścieżke do załącznika.");

            return localImagePath;
        }

        private string GetLocalImagePath(string imageUrl)
        {
            string localFilePath = "";
            string fileName = imageUrl.Split('/').Last();

            if (!System.IO.Directory.Exists(IMAGES_DIRECTORY))
            {
                Log.WriteErrorLog("Stworzono nowy folder.");
                System.IO.Directory.CreateDirectory(IMAGES_DIRECTORY);
            }

            localFilePath = "../../images/" + fileName;

            localFilePath = System.IO.Path.GetFullPath(localFilePath);
            Log.WriteErrorLog("Zwrócono ścieżke do pliku.");

            return localFilePath;
            }
        
    }
}
