﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class SchoolContext : DbContext
    {
        public SchoolContext() : base()
        {

        }

        public DbSet<Zadanie> Zadania { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<adresURL> Adresy { get; set; }

        public static implicit operator EnvironmentVariableTarget(SchoolContext v)
        {
            throw new NotImplementedException();
        }
    }
}
