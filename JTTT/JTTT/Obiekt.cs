﻿
using System;
using System.Runtime.Serialization;

namespace JTTT
{
    [Serializable]
    public class Obiekt:ISerializable
    {
        public string adres_url;
        public string adres_email;
        public string fraza;
        public string task_nazwa;
        public string zalacznik;

        public Obiekt()
        { }
        public Obiekt(string nazwa, string url, string fraza, string email)
        {
            Obiekt_url = url;
            Obiekt_email = email;
            Obiekt_fraza = fraza;
            Obiekt_task = nazwa;
        }
        public string Obiekt_zalacznik
        {
            get { return zalacznik; }
            set { zalacznik = value; }
        }
        public string Obiekt_url
        {
            get { return adres_url; }
            set { adres_url = value; }
        }

        public string Obiekt_email
        {
            get { return adres_email; }
            set { adres_email = value; }
        }
        public string Obiekt_fraza
        {
            get { return fraza; }
            set { fraza = value; }
        }
        public string Obiekt_task
        {
            get { return task_nazwa; }
            set { task_nazwa = value; }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}