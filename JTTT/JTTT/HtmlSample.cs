﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Text;


namespace JTTT
{
    public class HtmlSample
    {
        private readonly string _url;
        public string cos;

        public HtmlSample(string url)
        {
            this._url = url;
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));

                return html;
            }
        }

        
        public String PrintPageNodes(String text_include)
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml();
            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants("img");
            Obiekt obiekt = new Obiekt();
            foreach (var node in nodes)
            {
                
                if (node.GetAttributeValue("alt","").Contains(text_include))
                {
                    Console.WriteLine("MAM !!!!!!!!!!!!!!!!!!!!!!");
                    Console.WriteLine("---------");
                    Console.WriteLine("Src value: " + node.GetAttributeValue("src", ""));
                    obiekt.zalacznik = node.GetAttributeValue("src", "").ToString();
                    Console.WriteLine("Alt value: " + node.GetAttributeValue("alt", ""));      
                }
            }
            return obiekt.zalacznik;
        }
    }
}