﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Drawing;

namespace JTTT
{
    public partial class Form1 : Form
    {
        String adres;
        Log log;
        Obiekt obiekt;
        public int licznik;
        public BindingList<Obiekt> listOfObiekts;
        private Bitmap MyImage;
        SchoolContext Ctx = new SchoolContext();

        public string temperatura;

        public Form1()
        {
            licznik = 0;
            InitializeComponent();
            this.Load += new EventHandler(Form1_Load);
        }

        void Form1_Load(object sender, EventArgs e)
        {
            InitializeListOfObiekts();
            tasks_list.DisplayMember = "Obiekt_task";
            listOfObiekts.AddingNew += new AddingNewEventHandler(listOfObiekts_AddingNew);
            listOfObiekts.ListChanged += new ListChangedEventHandler(listOfObiekts_ListChanged);
        }

        void listOfObiekts_AddingNew(object sender, AddingNewEventArgs e)
        {
            e.NewObject = new Obiekt(task_name.Text, URL.Text, text_include.Text, email.Text);
        }

        void listOfObiekts_ListChanged(object sender, ListChangedEventArgs e)
        {
            MessageBox.Show(e.ListChangedType.ToString());
        }

        public void InitializeListOfObiekts()
        {
            listOfObiekts = new BindingList<Obiekt>();
            listOfObiekts.AllowNew = true;
            listOfObiekts.AllowRemove = false;
            listOfObiekts.RaiseListChangedEvents = true;
            listOfObiekts.AllowEdit = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            adres = URL.Text;

        }

        public void wyslij_email(string Server, String odbiorca, String ad_url, String fraza)
        {
            String zalacznik;
            log = new Log();
            try
            {
                var hs = new HtmlSample(ad_url);
                zalacznik = hs.PrintPageNodes(fraza);
                Console.Read();
                Download download = new Download();
                SmtpClient client_smtp = new SmtpClient(Server, 587);
                client_smtp.EnableSsl = true;
                client_smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                client_smtp.UseDefaultCredentials = false;
                client_smtp.Credentials = new System.Net.NetworkCredential("fakeKontoqOOp@gmail.com", "fakekonto1");
                MailMessage msg = new MailMessage();
                msg.To.Add(new MailAddress(odbiorca));
                msg.From = new MailAddress("fakeKontoqOOp@gmail.com");
                msg.Subject = fraza;
                msg.Body = "Zdjęcia zawierające frazę: " + fraza;
                Attachment zdj;
                zdj = new Attachment(@download.DownloadImageAndGetLocalPath(zalacznik));
                msg.Attachments.Add(zdj);
                client_smtp.Send(msg);
                MessageBox.Show("E-mail wysłano pomyślnie !");
                Log.WriteErrorLog("E-Mail wysłano pomyślnie.");
            }
            catch (Exception ex)
            {
                Log.WriteErrorLog("Błąd wysyłania maila " + ex.Message);
            }
        }

        private void wykonaj_Click(object sender, EventArgs e)
        {
            obiekt = new Obiekt();
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(tasks_list);
            selectedItems = tasks_list.SelectedItems;
            if (tasks_list.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    WebClient client_web = new WebClient();
                    wyslij_email("smtp.gmail.com", listOfObiekts[tasks_list.Items.IndexOf(selectedItems[i])].adres_email, listOfObiekts[tasks_list.Items.IndexOf(selectedItems[i])].adres_url, listOfObiekts[tasks_list.Items.IndexOf(selectedItems[i])].fraza);
                }
            }
            else
            {
                MessageBox.Show("Nie wybrano elementu !");
                Log.WriteErrorLog("Nie wybrano elementu.");
            }
        }

        private void add_to_list_Click(object sender, EventArgs e)
        {
            Obiekt newObiekt = listOfObiekts.AddNew();
            tasks_list.Items.Add("Task name = " + newObiekt.task_nazwa + ". " + "URL = " + newObiekt.adres_url + ". " + "Tekst = " + newObiekt.fraza + ". " + "Adres e-mail = " + newObiekt.adres_email + ".");
    
                Email email = new Email() { EmailAdress = newObiekt.adres_email };
                Zadanie zadanie = new Zadanie() { NazwaZadania = newObiekt.fraza };
            
                adresURL adres = new adresURL() { nazwaAdresuURL = newObiekt.adres_url };

                Ctx.Zadania.Add(zadanie);
                Ctx.Emails.Add(email);
                Ctx.Adresy.Add(adres);

                Ctx.SaveChanges();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(tasks_list);
            selectedItems = tasks_list.SelectedItems;
            if (tasks_list.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                    tasks_list.Items.Remove(selectedItems[i]);
            }
            else
            {
                MessageBox.Show("Nie wybrano elementu !");
                Log.WriteErrorLog("Nie wybrano elementu.");
            }
        }

        private void deserialize_Click(object sender, EventArgs e)
        {
        }

        private void clear_all_Click(object sender, EventArgs e)
        {
            tasks_list.Items.Clear();
        }

        private void serialize_Click(object sender, EventArgs e)
        {
            obiekt = new Obiekt();
            ListBox.SelectedObjectCollection selectedItems = new ListBox.SelectedObjectCollection(tasks_list);
            selectedItems = tasks_list.SelectedItems;
            SerializeClass serial = new SerializeClass();
            BinaryFormatter serializer = new BinaryFormatter();
            Stream strm = File.Create("E:/MyObjDir");

          
            
            if (tasks_list.SelectedIndex != -1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    //serial.Serialize(listOfObiekts[tasks_list.Items.IndexOf(selectedItems[i])]);
                    serializer.Serialize(strm, listOfObiekts[tasks_list.Items.IndexOf(selectedItems[i])]);
                }
            }
            else
            {
                MessageBox.Show("Nie wybrano elementu !");
                Log.WriteErrorLog("Nie wybrano elementu.");
            }
                 }


        private void button1_Click_1(object sender, EventArgs e)
        {
            GetWeatherData pogoda = new GetWeatherData(city_to_compare.Text);
            textBox1.Text= pogoda.getTemperature().ToString();
            Download down = new Download();
            MessageBox.Show(pogoda.ReturnWeatherInfo());

            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            
            MyImage = new Bitmap(down.DownloadImageAndGetLocalPath(pogoda.ReturnImageURL()));
            pictureBox1.ClientSize = new Size(50, 50);
            pictureBox1.Image = (Image)MyImage;
        }
    }
}