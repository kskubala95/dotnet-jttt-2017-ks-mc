﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace JTTT
{
    public class GetWeatherData
    {
        private string miasto;
        private RootObject WeatherJson = new RootObject();
        private string API1;
        public const string TOKEN = "d70dd7a22e5341c5d2a2ea9e135ca258";
        public static string iconURL = "http://openweathermap.org/img/w/";
        public GetWeatherData(string city)
        {

            miasto = city;
            API1 = "http://api.openweathermap.org/data/2.5/weather?q=" + miasto + ",pl&APPID=";
            GetWeatherFromCity();
        }

        public double getTemperature()
        {
            double temperatura = WeatherJson.main.temp - 273.15;
            return temperatura;
        }

        public string ReturnWeatherInfo()
        {
            double temp = WeatherJson.main.temp - 273.15;
            return "Dzisiaj w miescie " + miasto + " jest " + temp + " stopni Celsjusza, ciśnienie "
                + WeatherJson.main.pressure + " hPa. Niebo: " + WeatherJson.weather[0].description;
        }

        public String ReturnImageURL()
        {
            return iconURL + WeatherJson.weather[0].icon + ".png"; 
        }

        private void GetWeatherFromCity()
        {
            var json = "";
            using (var wc = new System.Net.WebClient())
            {
                String adres = API1 + TOKEN;
                json = wc.DownloadString(adres);
            }

            WeatherJson = JsonConvert.DeserializeObject<RootObject>(json);
        }
    }
}
